
Description de la méthode utilisée pour l'automatisation de chacuns des tests:

 * Orientation du fantôme: en axial, trouver le centre du cercle sur la première et 
	dernière tranche d'image et valider que 2 des 3 degrés de rotation sont à l'intérieur d'un
	seuil acceptable. Ensuite, on regarde l'objet interne du fantôme permettant l'évaluation
	de l'épaisseur de tranche et on mesure l'angle de la ligne identifiant la partie supérieure du 
	bloc. Cet angle doit être sous un seuil de tolérance.
 * Slice Position Accuracy: sur les images 1 et 11, on effectue le test tel que décrit dans le 
	document de l'ACR. Si la grille d'acquisition est mal placée, certains tests sont omis.
 * Fréquence de l'antenne: permet de suivre l'évolution de la fréquence utilisée pour l'antenne de
	tête.
 * Geometric accuracy sagittal: mesure de la hauteur du fantôme en sagittal tout juste à gauche du
	milieu de ce dernier.
 * Geometric Accuracy axial: sur les images 1 et 5. Trouver le centre du cercle, pour chacuns des
	360 degrés, trouver la distance entre le centre et la bordure du cercle. Trouver la longueur
	de chaque diamètre et conserver la mesure la plus loin de la valeure réelle.
 * High-contrast Spatial Resolution: caractériser le bruit dans le module en question. Pour chaque 
	bloc de résolution, balayer les colonnes et les rangées et déterminer si on trouve au moins une
	colonne et une rangée qui contiennent 4 pics qui se distinguent du bruit de fond.
 * Slice Thickness Accuracy: pour chacune des deux bandes du module, récupérer quelques rangées et 
	les combiner pour former un vecteur de signal. Déterminer la largeur à mi-hauteur sans prendre
	pour acquis une forme de courbe particulière (par exemple si on trouve un long plateau de signal
	constant). Effectuer le calcul comme dans le document de l'ACR.
 * Image Intensity Uniformity: sur l'image 7, approximation de l'inhomogénéité dans la section
	homogène du fantôme avec approximation par polynôme tel que décrit dans:
		Leemput, Van. (1999). Automated Model-Based Bias Field Correction of MR Images of the Brain. 
		IEEE TRANSACTIONS ON MEDICAL IMAGING. 18 (10), p885-895.
	On utilise ensuite la plus petite et la plus grande valeure de ce champ de biais pour poursuivre 
	le calcul.
 * Percent Signal Ghosting: La mesure est effectuée comme demandé dans le document de l'ACR. 
	La dimension et la position des boîtes s'ajustent en fonction des marges disponibles tel que 
	demandé par l'ACR.
 * Low Contrast Object Detectability (LCD): Ce test n'est pas effectué tel que spécifié dans le document 
	de l'ACR. Il existe une corrélation forte entre le SNR et le LCD. Au CICL, on a décidé de 
	déterminer cette corrélation afin que le seuil sur le compte des cercles puisse être remplacé
	par un seuil sur le SNR. La corrélation entre le SNR et le test de LCD a été discuté et utilisé
	par plusieurs autres physiciens médicaux: Keener, Carl R. et Chen Lin entre autres.