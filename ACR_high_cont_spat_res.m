%% Par: Bruno Carozza
%% Cr�� le: 26 mars 2015
%% Modifi� le: 13 mai 2015
%% Description: D�termine la r�solution maximale dans les 2 axes X et Y
%% R�sultats: ordre > 1.1X, 1.0X, 0.9X, 1.1Y, 1.0Y, 0.9Y
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function high_contrast_spatial_res = ACR_high_cont_spat_res(img, params_struct, pos_cercle_x, pos_cercle_y, ps, interpolation_factor)

    img_size = size(img);    
    ftm_diametre_px = params_struct.phantom_diam_mm/ps(2);

    % Cr�er un masque de la r�gion rectangulaire centrale
    mask_top = round(pos_cercle_y + params_struct.element_res_boxminy_mm/ps(1));
    mask_btm = round(pos_cercle_y + params_struct.element_res_boxmaxy_mm/ps(1));
    mask_left = round(pos_cercle_x + params_struct.element_res_boxminx_mm/ps(1));
    mask_right = round(pos_cercle_x + params_struct.element_res_boxmaxx_mm/ps(1));

    mask_rect = img;
    mask_rect(:) = 0;
    mask_rect(mask_top:mask_btm, mask_left:mask_right) = 1;

    data = double(mask_rect).*double(img);
    data = data(mask_top:mask_btm,mask_left:mask_right);
    
    %Position des grilles
    GrillPos_mm  = [13 36 60]*0.9766;
    GrillPos_Vox = GrillPos_mm/ps(1);
    
    data_11mm = data(:, round(GrillPos_Vox(1)) - 10:round(GrillPos_Vox(1)) + 10);
    data_10mm = data(:, round(GrillPos_Vox(2)) - 10:round(GrillPos_Vox(2)) + 10);
    data_09mm = data(:, round(GrillPos_Vox(3)) - 10:round(GrillPos_Vox(3)) + 10);
    
    [Xq,Yq] = meshgrid( 1:1/interpolation_factor:size(data_11mm,2),1:1/interpolation_factor:size(data_11mm,1));
    data_interp = interp2(1:size(data,2), 1:size(data,1), double(data),Xq, Yq, 'spline');
    data_11mm_interp = interp2(1:size(data_11mm,2), 1:size(data_11mm,1), double(data_11mm),Xq, Yq, 'spline');
    data_10mm_interp = interp2(1:size(data_11mm,2), 1:size(data_11mm,1), double(data_10mm),Xq, Yq, 'spline');
    data_09mm_interp = interp2(1:size(data_11mm,2), 1:size(data_11mm,1), double(data_09mm),Xq, Yq, 'spline');
        
    %%D�terminer la variance et la moyenne du bruit de fond
    img_bruit_haut = data_interp(1:round(4*interpolation_factor*0.9766/ps(1)),:);
    img_bruit_bas = data_interp(end-round(3*interpolation_factor*0.9766/ps(1)):end,1:end);
    vct_bruit_haut = reshape(img_bruit_haut, [size(img_bruit_haut,1)*size(img_bruit_haut,2) 1]);
    vct_bruit_bas = reshape(img_bruit_bas, [size(img_bruit_bas,1)*size(img_bruit_bas,2) 1]);
    vct_bruit = [vct_bruit_haut', vct_bruit_bas'];
    noise_range = max(vct_bruit)-min(vct_bruit);
    noise_mean = mean(vct_bruit);
    y11_has_4_peaks = 0;
    x11_has_4_peaks = 0;
    y10_has_4_peaks = 0;
    x10_has_4_peaks = 0;
    y09_has_4_peaks = 0;
    x09_has_4_peaks = 0;
    warning off;
    
    %%Pour chacun des modules de r�solution, terminer si on peux r�soudre 4
    %%points dans les 2 axes.
    for i=1:size(data_11mm_interp,1)
        vct = data_11mm_interp(i,:);
        [peaks, locs] = findpeaks(vct, 1:size(vct,2), 'MinPeakHeight', noise_mean + noise_range, ...
            'MinPeakWidth', 0.7*interpolation_factor, ...
            'MinPeakDistance', 0.9*interpolation_factor, 'MinPeakProminence', noise_mean + noise_range);
        if(length(peaks) >= 4)
            y11_has_4_peaks = 1;
        end
    end
    for i=1:size(data_11mm_interp,2)
        vct = data_11mm_interp(:,i);
        [peaks, locs] = findpeaks(vct, 1:size(vct,2), 'MinPeakHeight', noise_mean + noise_range, ...
            'MinPeakWidth', 0.7*interpolation_factor, ...
            'MinPeakDistance', 0.9*interpolation_factor, 'MinPeakProminence', noise_mean + noise_range);
        if(length(peaks) >= 4)
            x11_has_4_peaks = 1;
        end
    end
    
    for i=1:size(data_10mm_interp,1)
        vct = data_10mm_interp(i,:);
        [peaks, locs] = findpeaks(vct, 1:size(vct,2), 'MinPeakHeight', noise_mean + noise_range, ...
            'MinPeakWidth', 0.7*interpolation_factor, ...
            'MinPeakDistance', 0.9*interpolation_factor, 'MinPeakProminence', noise_mean + noise_range);
        if(length(peaks) >= 4)
            y10_has_4_peaks = 1;
        end
    end
    for i=1:size(data_10mm_interp,1)
        vct = data_10mm_interp(i,:);
        [peaks, locs] = findpeaks(vct, 1:size(vct,2), 'MinPeakHeight', noise_mean + noise_range, ...
            'MinPeakWidth', 0.7*interpolation_factor, ...
            'MinPeakDistance', 0.9*interpolation_factor, 'MinPeakProminence', noise_mean + noise_range);
        if(length(peaks) >= 4)
            x10_has_4_peaks = 1;
        end
    end
    
    for i=1:size(data_09mm_interp,2)
        vct = data_09mm_interp(:,i);
        [peaks, locs] = findpeaks(vct, 1:size(vct,2), 'MinPeakHeight', noise_mean + noise_range, ...
            'MinPeakWidth', 0.7*interpolation_factor, ...
            'MinPeakDistance', 0.7*interpolation_factor, 'MinPeakProminence', noise_mean + noise_range/2);
        if(length(peaks) >= 4)
            y09_has_4_peaks = 1;
        end
    end
    for i=1:size(data_09mm_interp,1)
        vct = data_09mm_interp(i,:);
        [peaks, locs] = findpeaks(vct, 1:size(vct,2), 'MinPeakHeight', noise_mean + noise_range, ...
            'MinPeakWidth', 0.7*interpolation_factor, ...
            'MinPeakDistance', 0.7*interpolation_factor, 'MinPeakProminence', noise_mean + noise_range/2);
        if(length(peaks) >= 4)
            x09_has_4_peaks = 1;
        end
    end
    warning on;
    
    high_contrast_spatial_res = 99;
    
    if(x09_has_4_peaks == 1 && y09_has_4_peaks == 1)
        high_contrast_spatial_res = 0.9;
    elseif(x10_has_4_peaks == 1 && y10_has_4_peaks == 1)
        high_contrast_spatial_res = 1.0;
    elseif(x11_has_4_peaks == 1 && y11_has_4_peaks == 1)
        high_contrast_spatial_res = 1.1;
    end
end