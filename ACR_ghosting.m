%% Par: Bruno Carozza
%% Cr�� le: 2 avril 2015
%% Description: D�termine le Percent Signal Ghosting dans le fant�me
%% R�sultats: PSG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function percent_signal_ghosting = ACR_ghosting(img_7, params_struct, pos_cercle_x, pos_cercle_y, rayon_cercle, ps)
    
    img_7 = img_7 + 1;
    
    % Extraire les caract�ristiques de l'image    
    img_size = size(img_7);    
    ftm_diametre_mm = params_struct.phantom_diam_mm;      
    
    %% D�terminer l'intensit� moyenne du signal
    %%Trouver un rayon en pixels pour lequel la surface du cercle de m�me
    %%centre sera d'environ 200 cm^2 ou 20 000 mm^2. 
    pi = 3.141592;
    r = sqrt(20000/ps(1)/pi)*ps(1);
    
    %%D�terminer des points d�finissants le contour de ce cercle pour cr�er
    %%un masque
    [H,X,Y] = circle([pos_cercle_x pos_cercle_y],r,1000,'-w',0);
    
    %%Cr�er un masque � partir du centre du fant�me et du nouveau rayon
    %%ayant la forme circulaire. Les voxels appartenants � ce masque seront
    %%ceux utilis�s pour d�terminer l'uniformit� de l'intensit�... ou
    %%plut�t le bias field (BF)
    ROI = roipoly(1:img_size(1), 1:img_size(2), img_7, X, Y);
    
    ROI_data = double(img_7(ROI));
    
    
    %%fitter l'inhomog�n�it� du signal par un polynome 2D de degr� 3 ou 4
    %%(Van Leemput) afin d'en tirer la valeure maximal et minimale du
    %%signal sans tenir compte du bruit.
    pos = [];
    [pos(:,1),pos(:,2)] = find(ROI);    
    
    
    % Trouver le poids d'appartenance de chaque pixel au ROI
    param = fitdist(ROI_data, 'rician');
    pd = makedist('Rician', 's', param.s, 'sigma', param.sigma);
    temp = pdf(pd, ROI_data);
    w = temp./max(temp);
    
    %w  = ones([sum(ROI(:)>0) 1]);      
    v  = var(ROI_data);
    
    
    %%Calcul du mean signal intensity
    ROI_mean_signal = sum(ROI_data(:,1).*w(:,1))/sum(w);
        
    %%D�terminer l'�paisseur des 4 ROI autour du fantome dans la direction
    %%radiale en tenant compte qu'il faut que ces derniers respectent
    %%certains crit�res tels ne pas �tre en contact avec la bordure de
    %%l'image et du fant�me.
    
    %dimensions des ROIs en mm
    dim_ROI_1           = 15.8114;
    dim_ROI_2           = 63.2456;
    
    %dimensions des ROIs en voxels
    dim_ROI_1           = dim_ROI_1/ps(1);
    dim_ROI_2           = dim_ROI_2/ps(1);
    
    %distances minimales entre les ROI et le fantome/bordure de l'image en
    %voxels
    min_dist_phantom    = 5/ps(1);
    min_dist_border     = 5;
    
    %%%%%%%%%%%%%%%%% ROI 1 %%%%%%%%%%%%%%%%%%%%
    %Espace disponible entre le fant�me et la bordure de l'image
    dist_dispo          = round(pos_cercle_y - rayon_cercle - min_dist_phantom - min_dist_border);
    dist_requise        = round(dim_ROI_1);
    if dist_dispo < dist_requise
        %dimensions des ROIs en mm
        dim_ROI_11       = dist_dispo*ps(1);
        dim_ROI_22       = 1000/dim_ROI_11;
        %dimensions des ROIs en voxels
        dim_ROI_11       = dim_ROI_11/ps(1);
        dim_ROI_22       = dim_ROI_22/ps(1);
    else
        dim_ROI_11       = dim_ROI_1;
        dim_ROI_22       = dim_ROI_2;
    end
    
    ROI_haut_y_max      = round(pos_cercle_y - rayon_cercle - min_dist_phantom);
    ROI_haut_y_min      = round(ROI_haut_y_max - dim_ROI_11);
    
    ROI_haut_x_min      = round(pos_cercle_x - dim_ROI_22/2);
    ROI_haut_x_max      = round(pos_cercle_x + dim_ROI_22/2);
    
    ROI1 = zeros(img_size);
    ROI1(ROI_haut_y_min:ROI_haut_y_max,ROI_haut_x_min:ROI_haut_x_max) = 1;
    
    %%%%%%%%%%%%%%%%% ROI 2 %%%%%%%%%%%%%%%%%%%%
    %Espace disponible entre le fant�me et la bordure de l'image
    dist_dispo          = img_size(1) - round(pos_cercle_x + rayon_cercle + min_dist_phantom + min_dist_border);
    dist_requise        = round(dim_ROI_1);
    if dist_dispo < dist_requise
        %dimensions des ROIs en mm
        dim_ROI_11       = dist_dispo*ps(1);
        dim_ROI_22       = 1000/dim_ROI_11;
        %dimensions des ROIs en voxels
        dim_ROI_11           = dim_ROI_11/ps(1);
        dim_ROI_22           = dim_ROI_22/ps(1);
    else
        dim_ROI_11       = dim_ROI_1;
        dim_ROI_22       = dim_ROI_2;
    end
    
    ROI_droite_y_max      = round(pos_cercle_y + dim_ROI_22/2);
    ROI_droite_y_min      = round(pos_cercle_y - dim_ROI_22/2);
    
    ROI_droite_x_min      = round(pos_cercle_x + rayon_cercle + min_dist_phantom);
    ROI_droite_x_max      = round(ROI_droite_x_min + dim_ROI_11);
    
    ROI2 = zeros(img_size);
    ROI2(ROI_droite_y_min:ROI_droite_y_max,ROI_droite_x_min:ROI_droite_x_max) = 1;
    
    %%%%%%%%%%%%%%%%% ROI 3 %%%%%%%%%%%%%%%%%%%%
    %Espace disponible entre le fant�me et la bordure de l'image
    dist_dispo          = img_size(2) - round(pos_cercle_y + rayon_cercle + min_dist_phantom + min_dist_border);
    dist_requise        = round(dim_ROI_1);
    if dist_dispo < dist_requise
        %dimensions des ROIs en mm
        dim_ROI_11       = dist_dispo*ps(1);
        dim_ROI_22       = 1000/dim_ROI_11;
        %dimensions des ROIs en voxels
        dim_ROI_11       = dim_ROI_11/ps(1);
        dim_ROI_22       = dim_ROI_22/ps(1);
    else
        dim_ROI_11       = dim_ROI_1;
        dim_ROI_22       = dim_ROI_2;
    end
    
    ROI_bas_y_min      = round(pos_cercle_y + rayon_cercle + min_dist_phantom);
    ROI_bas_y_max      = round(ROI_bas_y_min + dim_ROI_11);
    
    ROI_bas_x_min      = round(pos_cercle_x - dim_ROI_22/2);
    ROI_bas_x_max      = round(pos_cercle_x + dim_ROI_22/2);
    
    ROI3 = zeros(img_size);
    ROI3(ROI_bas_y_min:ROI_bas_y_max,ROI_bas_x_min:ROI_bas_x_max) = 1;
    
    %%%%%%%%%%%%%%%%% ROI 4 %%%%%%%%%%%%%%%%%%%%
    %Espace disponible entre le fant�me et la bordure de l'image
    dist_dispo          = img_size(1) - round(pos_cercle_x + rayon_cercle + min_dist_phantom + min_dist_border);
    dist_requise        = round(dim_ROI_1);
    if dist_dispo < dist_requise
        %dimensions des ROIs en mm
        dim_ROI_11       = dist_dispo*ps(1);
        dim_ROI_22       = 1000/dim_ROI_11;
        %dimensions des ROIs en voxels
        dim_ROI_11           = dim_ROI_11/ps(1);
        dim_ROI_22           = dim_ROI_22/ps(1);
    else
        dim_ROI_11       = dim_ROI_1;
        dim_ROI_22       = dim_ROI_2;
    end
    
    ROI_gauche_y_max      = round(pos_cercle_y + dim_ROI_22/2);
    ROI_gauche_y_min      = round(pos_cercle_y - dim_ROI_22/2);
    
    ROI_gauche_x_max      = round(pos_cercle_x - rayon_cercle - min_dist_phantom);
    ROI_gauche_x_min      = round(ROI_gauche_x_max - dim_ROI_11);
    
    ROI4 = zeros(img_size);
    ROI4(ROI_gauche_y_min:ROI_gauche_y_max,ROI_gauche_x_min:ROI_gauche_x_max) = 1;
    
    %% S'assurer que les 4 ROI existe �tant donn� qu'il y a assez d'espace
    %% de chaque c�t�s
    if sum(ROI1(:)) == 0
        ROI1 = ROI3;
    end
    if sum(ROI3(:)) == 0
        ROI3 = ROI1;
    end
    if sum(ROI2(:)) == 0
        ROI2 = ROI4;
    end
    if sum(ROI4(:)) == 0
        ROI4 = ROI2;
    end
    
    %%Mesurer le "ghosting ratio"
    ROI1_mean       = sum(ROI1(:) .* double(img_7(:)))/sum(ROI1(:));
    ROI2_mean       = sum(ROI2(:) .* double(img_7(:)))/sum(ROI2(:));
    ROI3_mean       = sum(ROI3(:) .* double(img_7(:)))/sum(ROI3(:));
    ROI4_mean       = sum(ROI4(:) .* double(img_7(:)))/sum(ROI4(:));
    
    
    ghosting_ratio  = abs(((ROI1_mean+ROI3_mean) - (ROI4_mean+ROI2_mean))/(2*ROI_mean_signal));
    percent_signal_ghosting = ghosting_ratio * 100;
    
    
end