function [methodinfo,structs,enuminfo,ThunkLibName]=ImageProcessing
%IMAGEPROCESSING Create structures to define interfaces found in 'visible'.

%This function was generated by loadlibrary.m parser version  on Wed Apr 15 08:38:11 2015
%perl options:'visible.i -outfile=ImageProcessing.m -thunkfile=ImageProcessing_thunk_pcwin64.c -header=visible.h'
ival={cell(1,0)}; % change 0 to the actual number of functions to preallocate the data.
structs=[];enuminfo=[];fcnNum=1;
fcns=struct('name',ival,'calltype',ival,'LHS',ival,'RHS',ival,'alias',ival,'thunkname', ival);
MfilePath=fileparts(mfilename('fullpath'));
ThunkLibName=fullfile(MfilePath,'ImageProcessing_thunk_pcwin64');
%  double atan360 ( double x , double y ); 
fcns.thunkname{fcnNum}='doubledoubledoubleThunk';fcns.name{fcnNum}='atan360'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='double'; fcns.RHS{fcnNum}={'double', 'double'};fcnNum=fcnNum+1;
%  double * Gradient ( int * ImgArrayIn , int ImgArrayDim [], int Axe ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrvoidPtrint32Thunk';fcns.name{fcnNum}='Gradient'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'int32Ptr', 'int32Ptr', 'int32'};fcnNum=fcnNum+1;
%  double * CircleDetection ( int * ImgArrayIn , int ImgArrayDim [], int minRayon ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrvoidPtrint32Thunk';fcns.name{fcnNum}='CircleDetection'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'int32Ptr', 'int32Ptr', 'int32'};fcnNum=fcnNum+1;
%  double BilinearInterp ( int * ImgArrayIn , int ImgArrayDim [], double PosX , double PosY ); 
fcns.thunkname{fcnNum}='doublevoidPtrvoidPtrdoubledoubleThunk';fcns.name{fcnNum}='BilinearInterp'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='double'; fcns.RHS{fcnNum}={'int32Ptr', 'int32Ptr', 'double', 'double'};fcnNum=fcnNum+1;
%  double * RadialEdgeDetection ( int * ImgArrayIn , int ImgArrayDim [], double PosXCercle , double PosYCercle , double Rayon , double Angle ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrvoidPtrdoubledoubledoubledoubleThunk';fcns.name{fcnNum}='RadialEdgeDetection'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'int32Ptr', 'int32Ptr', 'double', 'double', 'double', 'double'};fcnNum=fcnNum+1;
%  double * RadialVct ( int * ImgArrayIn , int ImgArrayDim [], double PosXCercle , double PosYCercle , double Rayon , double Angle ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrvoidPtrdoubledoubledoubledoubleThunk';fcns.name{fcnNum}='RadialVct'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'int32Ptr', 'int32Ptr', 'double', 'double', 'double', 'double'};fcnNum=fcnNum+1;
%  double * Filter1D ( double * data , int data_dim , int hSize ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrint32int32Thunk';fcns.name{fcnNum}='Filter1D'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'doublePtr', 'int32', 'int32'};fcnNum=fcnNum+1;
%  double * Diff1D ( double * data , int data_dim , int diffOrder , bool kernelType ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrint32int32boolThunk';fcns.name{fcnNum}='Diff1D'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'doublePtr', 'int32', 'int32', 'bool'};fcnNum=fcnNum+1;
%  int * ImgRet ( int PosX , int PosY ); 
fcns.thunkname{fcnNum}='voidPtrint32int32Thunk';fcns.name{fcnNum}='ImgRet'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='int32Ptr'; fcns.RHS{fcnNum}={'int32', 'int32'};fcnNum=fcnNum+1;
%  double EdgeDetection1D ( double * VctIn , int VctDim , bool EdgeCond ); 
fcns.thunkname{fcnNum}='doublevoidPtrint32boolThunk';fcns.name{fcnNum}='EdgeDetection1D'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='double'; fcns.RHS{fcnNum}={'doublePtr', 'int32', 'bool'};fcnNum=fcnNum+1;
%  double * LocalExtremum1D ( int * VctIn , int VctDim , double PeakTresh , bool MinMax , double PeakNeighTresh ); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrint32doublebooldoubleThunk';fcns.name{fcnNum}='LocalExtremum1D'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='doublePtr'; fcns.RHS{fcnNum}={'int32Ptr', 'int32', 'double', 'bool', 'double'};fcnNum=fcnNum+1;
%  int * QC_HighContrastSpatialRes ( int * ImgIn , int ImgDim [ 2 ], double PeakTresh , double PeakWidthMax , double PeakWidthMin , double PeakNeighTresh , double DistInterPeakMax , double DistInterPeakMin , int GrillePos [ 3 ]); 
fcns.thunkname{fcnNum}='voidPtrvoidPtrvoidPtrdoubledoubledoubledoubledoubledoublevoidPtrThunk';fcns.name{fcnNum}='QC_HighContrastSpatialRes'; fcns.calltype{fcnNum}='Thunk'; fcns.LHS{fcnNum}='int32Ptr'; fcns.RHS{fcnNum}={'int32Ptr', 'int32Ptr', 'double', 'double', 'double', 'double', 'double', 'double', 'int32Ptr'};fcnNum=fcnNum+1;
methodinfo=fcns;