function [ pos ] = EdgeDetect1D( vct, dir )
    %dir = 0, noir au blanc
    %dir = 1, blanc au noir
    %vct_smooth = smooth(double(vct),15);
    vct = double(vct);
    vct_size = max(size(vct));
    vct_smooth = vct;
    interp_factor = 20;
    
    vct_interp = interp1(double(vct_smooth),1:1/interp_factor:vct_size, 'pchip');
    vct_interp_d = smooth(diff(double(vct_interp),1),15);
    if dir == 1
        [y, pos_interp] = max(vct_interp_d);
    else
        [y, pos_interp] = min(vct_interp_d);
    end
    
    pos = (pos_interp-1)/interp_factor + 1;

    
%     figure
%     hold on
%     plot(1:size(vct,2),vct, '-b')
%     plot(1:1/interp_factor:size(vct,2)-1/interp_factor,vct_interp_d, '-r')
%     plot(1:1/interp_factor:size(vct,2),vct_interp, '-g')
%     hold off
end