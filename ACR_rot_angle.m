%% Par: Bruno Carozza
%% Cr�� le: 21 avril 2015
%% Description: cette fonction d�tecte l'angle de rotation du fant�me
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [img_rot, angle_rot] = ACR_rot_angle(img, img_ps, pos_cercle_x, pos_cercle_y, interpolation_factor)

    % Cr�er un masque de la r�gion rectangulaire centrale
    mask_top = round(pos_cercle_y - 13/img_ps);
    mask_btm = round(pos_cercle_y + 13/img_ps);
    mask_left = round(pos_cercle_x - 87/img_ps);
    mask_right = round(pos_cercle_x + 87/img_ps);
    
    img_mask = img(mask_top:mask_btm, mask_left:mask_right);
    
    % Interpoler cette section de l'image pour une plus haute pr�cision sur l'angle de
    % rotation.
    [Xq,Yq] = meshgrid( 1:1/interpolation_factor:size(img_mask,2),1:1/interpolation_factor:size(img_mask,1));
    img_mask_interp = interp2(1:size(img_mask,2), 1:size(img_mask,1), double(img_mask),Xq, Yq, 'spline');
    
    %Trouver l'angle du rectangle central avec l'horizontale afin de
    %corriger
    object_lines = edge(int16(img_mask_interp),'canny'); % extract edges
    [H,T,R] = hough(object_lines);
    peaks = houghpeaks(H,2);
    lines = houghlines(object_lines, T, R, peaks);  
    
    %Passer au travers des lignes possibles afin d'en trouver une bonne
    cpt = 1;
    angle_rot = 0;
    while cpt<=size(lines,2) && angle_rot == 0
        temp = lines(cpt).point1 - lines(cpt).point2;
        angle_rot = atan(temp(2)/temp(1))/2/pi*360; 
        cpt = cpt + 1;
    end    
    
    img_rot = imrotate(img,angle_rot,'bilinear','crop');
    
end