%% Par: Bruno Carozza
%% Cr�� le: 7 avril 2015
%% Modifi� le 14 mai 2015
%% Description: D�termine le SNR sur la tranche 7 de l'ACR
%% R�sultats: SNR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function snr = ACR_snr_within_phantom(img_7, params_struct, pos_cercle_x, pos_cercle_y, rayon_cercle, ps)
    
    img_size = size(img_7);
    %%Trouver un rayon en pixels pour lequel la surface du cercle de m�me
    %%centre sera d'environ 200 cm^2 ou 20 000 mm^2. 
    pi = 3.141592;
    r = sqrt(20000/ps/pi)*ps;
    
    %%D�terminer des points d�finissants le contour de ce cercle pour cr�er
    %%un masque
    [H,X,Y] = circle([pos_cercle_x pos_cercle_y],r,1000,'-w',0);
    
    %%Cr�er un masque � partir du centre du fant�me et du nouveau rayon
    %%ayant la forme circulaire. Les voxels appartenants � ce masque seront
    %%ceux utilis�s pour d�terminer l'uniformit� de l'intensit�... ou
    %%plut�t le bias field (BF)
    ROI = roipoly(1:img_size(1), 1:img_size(2), img_7, X, Y);
    
    %Prendre le log des data
    ROI_data = double(img_7(ROI));
    
    
    %%fitter l'inhomog�n�it� du signal par un polynome 2D de degr� 3 ou 4
    %%(Van Leemput) afin d'en tirer la valeure maximal et minimale du
    %%signal sans tenir compte du bruit.
    pos = [];
    [pos(:,1),pos(:,2)] = find(ROI);    
    
    
    % Trouver le poids d'appartenance de chaque pixel au ROI
    param = fitdist(ROI_data, 'rician');
    pd = makedist('Rician', 's', param.s, 'sigma', param.sigma);
    temp = pdf(pd, ROI_data);
    w = temp./max(temp);
    
    %w  = ones([sum(ROI(:)>0) 1]);      
    v  = var(ROI_data);
    
    
    %%Calcul du mean signal intensity pour la d�termination du
    %%"percent signal ghosting"
    ROI_mean_signal = sum(ROI_data(:,1).*w(:,1))/sum(w);
    
    mu = ROI_mean_signal;
    
    % pour la normalisation du champ biais�
    ytnum = ROI_data/v*mu; 
    ytdeno = ROI_data/v;

    yt = sum(ytnum,2)./(sum(ytdeno,2)+eps);
    
    
    p = polyfitweighted2(single(pos(:,1)),single(pos(:,2)),ROI_data,single(yt),6,single(w));
    if sum(isnan(p))>0
        p = zeros([10 1]);
    end
    bf = polyval2(p,single(pos(:,1)),single(pos(:,2)));
    
    img_bf = double(img_7);
    img_bf(ROI==0) = 0;
    img_bf(ROI) = bf;
    
    img_roi_wout_bf = double(img_7).*double(ROI) - double(img_bf);
    
    % Mesurer le bruit dans un ROI du tier du rayon au centre du ROI
    [H,X,Y] = circle([pos_cercle_x pos_cercle_y],r/3,1000,'-w',0); 
    ROI_small = roipoly(1:img_size(1), 1:img_size(2), img_roi_wout_bf, X, Y);
    
    moy_signal = sum(img_roi_wout_bf(ROI_small))/sum(ROI_small(:));
    
    img_noise_SD            = std(img_roi_wout_bf(ROI_small));
    img_noise               = img_noise_SD;%/0.655; > seulement lorsqu'on prend le bruit dans � un endroit sans signal > dist ricienne
    
    %results.nstd = img_noise_SD;
    %results.smean = moy_signal;
    
    snr                     = moy_signal/img_noise;
    
end