function snr = ACR_snr_from_bg(img, info)

    % Find the average signal 
    moy_signal = median(img(img(:) > max(img(:))*2/3));

    % Pick background ROI vs Phase encoding (avoid ghosting)
    roi_size                  = 20/info.PixelSpacing(1);
    min_dist_border           = 3;
   
    if(strcmp(info.InPlanePhaseEncodingDirection, 'ROW'))
        roi_min_y = round(min_dist_border);
        roi_max_y = round(roi_min_y + roi_size);
        roi_min_x = round(min_dist_border);
        roi_max_x = round(roi_min_x + roi_size*5);
    else
        roi_min_y = round(min_dist_border);
        roi_max_y = round(roi_min_y + roi_size*5);
        roi_min_x = round(min_dist_border);
        roi_max_x = round(roi_min_x + roi_size);
    end
    
    ROI = zeros(size(img));
    ROI(roi_min_y:roi_max_y,roi_min_x:roi_max_x) = 1;
    
    img_noise_SD            = sqrt(var(ROI(:).*double(img(:))));
    img_noise               = img_noise_SD/0.66;
    
    snr                     = moy_signal/img_noise;
    
end