function [ result_struct ] = imPro_mri_acrphantom( directory )
    %imPro_mri_acrphantom Reads folder path for MRI image file, looks for required
    %files (see ACR test guidance), executes appropriate tests and build a
    %structure containing results.
    %   Spokes test replaced by a contrast to noise and signal to noise
    %   ratio calculation.
    % Limitations: if you have other sequences than the ACR'S ones, their
    %               series description must not have space in their name.
    % Version: 0.02 (alpha)
    % By: Bruno Carozza, M.Sc. Medical Physics
    % Email: bruno.carozza@gmail.com
    % Creation date: 2 mai 2015
    % Last edit date: 16 juin 2015
    
    clearvars -EXCEPT directory;
    
    result_struct = struct;
    
    %% Parameters
    % Phantom meta-data
    params_struct.phantom_diam_mm = 190;
    % Phantom positionning  offset tolerance(angle in degrees)
    params_struct.max_phantom_axis_rotation_angle = 1.4;
    params_struct.max_phantom_offaxis_rotation_angle = 1;
    % Acquisition grid positionning offset tolerance
    params_struct.max_longi_grid_pos_offset_mm = 4;
    % SNR tolerance 
    params_struct.min_snr = 30;                                          % SNR below 30 might cause issues with object/border detection
    params_struct.noise_filter_kernel_size_px = [7 7];
    params_struct.noise_filter_kernel_std_px = 0.8;
    % Resolution management vs partial volumes
    params_struct.interpolation_factor = 8;
    % Slice position element bounding box relative to inplane phantom center
    params_struct.element_slicepos_boxminx_mm = -4*0.9766;
    params_struct.element_slicepos_boxmaxx_mm = 4*0.9766;
    params_struct.element_slicepos_boxminy_mm = -91*0.9766;
    params_struct.element_slicepos_boxmaxy_mm = -46*0.9766;
    
    % Resolution element bounding box relative to inplane phantom center
    params_struct.element_res_boxminx_mm = -27*0.9766;
    params_struct.element_res_boxmaxx_mm = 54*0.9766;
    params_struct.element_res_boxminy_mm = 23*0.9766;
    params_struct.element_res_boxmaxy_mm = 54*0.9766;
    
    %% Get and validate data
    [files_struct, has_acr_loc, has_acr_t1, has_acr_t2, full_file_names] = GetRequiredFilesInfos(directory);

    % What are you working with
    if(isempty(fieldnames(files_struct)))
        warning('There are no useful files in the directory');
        return;
    end
    if(~has_acr_loc || ~has_acr_t1 || ~has_acr_t2)
        warning(strcat('Missing some of the required data for the daily QA. HaveSagLoc: ', num2str(has_acr_loc), ...
            ', HaveAxialT1: ',num2str(has_acr_t1), ', HaveAxialT2: ', num2str(has_acr_t2)));
    end
    
    [we_can_liftoff] = ValidateData(files_struct, params_struct);
    
    % Extract image data
    [data_struct] = DataPrep(files_struct, params_struct);
    
    %% Execute tests and return results
    if(we_can_liftoff)
        [result_struct] = MakeMeasurements(data_struct, params_struct);
    end
    
    %% Delete files
    for i=1:length(full_file_names)
        delete(full_file_names{i});
    end
end

function [files_struct, has_acr_loc, has_acr_t1, has_acr_t2, full_file_names] = GetRequiredFilesInfos(directory)
    %GetRequiredFilesPath. 1 level folder only. Looks for 1 sagittal 
    %localizer, 11 T1 and 11 T2 TE 80ms and any other serie with 11 slices 
    %(will delete T2 with TE=20ms). Will only analyse files from the first
    %study it encounters. Remaining files will be left in the folder.
    files_struct = struct;
    full_file_names = {};
    study_uid = '';
    
    files = dir(directory);
    for file = files'
        if(~strcmp(file.name,'.') && ~strcmp(file.name,'..'))
            full_file_name = fullfile(directory, file.name);
            full_file_names{end + 1} = full_file_name;
            try
                info = dicominfo(full_file_name);
            catch
                warning(strcat('GetRequiredFilesInfos > This is not a valid dicom file: ', full_file_name));
                continue;
            end
            
            if(isempty(study_uid))
                study_uid = info.StudyInstanceUID;
            elseif(~strcmp(study_uid, info.StudyInstanceUID))
                continue;
            end
            
            % Find data required for the ACR daily QA. If more than 1 sequence satisfies the
            % same conditions, the first one is taken as the ACR one the
            % other one will be used as a site specific sequence.
            if(strcmp(info.Modality, 'MR') && strcmp(info.MRAcquisitionType,'2D'))
                if(sum(info.ImageOrientationPatient - [0 1 0 0 0 -1]') == 0 && ...
                        info.RepetitionTime == 200 && info.EchoTime == 20)   % is Sagittal localizer
                    files_struct = AddFileToStruct(full_file_name, info, files_struct, 'ACR_Sagittal_Loc');
                elseif(info.SpacingBetweenSlices == 10 && info.SliceThickness == 5 && ...       % ACR required
                            sum(info.ImageOrientationPatient - [1 0 0 0 1 0]') == 0)                 % is Axial
                    if(info.EchoTime == 20 && info.RepetitionTime == 500 && ...
                            info.Rows == 256 && info.Columns == 256 && ...
                            strcmp(info.ScanningSequence, 'SE') && ...
                            any(info.InstanceNumber == [1 5 7 11]) && ...
                            GoesInACRSeq(files_struct, info, 'ACR_Axial_T1') && ...
                            isempty(strfind(info.ProtocolName, 'SUP')) && isempty(strfind(info.ProtocolName, 'INF')))      % is ACR T1
                        files_struct = AddFileToStruct(full_file_name, info, files_struct, 'ACR_Axial_T1');
                    elseif(info.RepetitionTime == 2000 && ...
                            info.Rows == 256 && info.Columns == 256 && ...
                            strcmp(info.ScanningSequence, 'SE') && ...
                            (any(info.InstanceNumber == [12 16 18 22]) || any(info.InstanceNumber == [1 5 7 11])) && ...
                            isempty(strfind(info.ProtocolName, 'SUP')) && isempty(strfind(info.ProtocolName, 'INF')))       % is ACR T2 
                        if(info.EchoTime == 80 && GoesInACRSeq(files_struct, info, 'ACR_Axial_T2'))
                            files_struct = AddFileToStruct(full_file_name, info, files_struct, 'ACR_Axial_T2');                       
                        end
                    elseif(any(info.InstanceNumber == [1 5 7 11]))                            % is site sequence
                        files_struct = AddFileToStruct(full_file_name, info, files_struct, '');                    
                    end               
                end           
            end
        end
    end
    
    % Verify that we have all data required by ACR (sag localizer, T1 and
    % T2)
    [has_acr_loc, has_acr_t1, has_acr_t2] = HasACRFiles(files_struct);
end

function [result_struct] = MakeMeasurements(data_struct, params_struct)
    result_struct = struct;
    %cpt_seq_t1 = find(arrayfun(@(n) strcmp(data_struct(n).seq_name, 'ACR_Axial_T1'), 1:numel(data_struct)));
    %cpt_seq_t2 = find(arrayfun(@(n) strcmp(data_struct(n).seq_name, 'ACR_Axial_T2'), 1:numel(data_struct)));
    %cpt_seq_loc = find(arrayfun(@(n) strcmp(data_struct(n).seq_name, 'ACR_Sagittal_Loc'), 1:numel(data_struct)));

    %% Loop on series
    for i=1:length(data_struct)
        
        temp = strfind(data_struct(i).seq_name,'_');
        if(~isempty(temp))
            short_seq_name = lower(data_struct(i).seq_name( temp(end)+1:end));
        else
            short_seq_name = strrep(strrep(data_struct(i).seq_name, ' ', '_'), '-', '_');
        end
        
        if(strcmp(data_struct(i).seq_name, 'ACR_Sagittal_Loc')) 
            %% Imaging Frequency
            result_struct.(strcat('imaging_frequency_',short_seq_name)) = data_struct(i).imaging_freq;
            
            %% Geometric distorsion
            % Sagittal
            result_struct.(strcat('phantom_height_',short_seq_name)) = ACR_geo_acc_sag( data_struct(i).img_1_filtered, data_struct(i).pixel_spacing);
        else
            %% QA Date and Time
            result_struct.dttm = data_struct(i).dttm;
            
            %% Slice Position
            result_struct.(strcat('slicepos_1_',short_seq_name)) = ACR_slice_pos(data_struct(i).img_1_rot, params_struct,...
                data_struct(i).img_1_centercoords(1), data_struct(i).img_1_centercoords(2),...
                data_struct(i).pixel_spacing(1));
            result_struct.(strcat('slicepos_11_',short_seq_name)) = ACR_slice_pos(data_struct(i).img_11_rot, params_struct,...
                data_struct(i).img_11_centercoords(1), data_struct(i).img_11_centercoords(2),...
                data_struct(i).pixel_spacing(1));
            
            %% Phantom Axis Rotation Angle
            result_struct.(strcat('phantom_axis_rot_angle_',short_seq_name)) = data_struct(i).angle_rot;
            
            %% Geometric distorsion
            % Axial
            result_struct.(strcat('phantom_diam_1_',short_seq_name)) = ACR_geo_acc_ax( data_struct(i).img_1_filtered, params_struct, ...
                data_struct(i).img_1_centercoords(1), data_struct(i).img_1_centercoords(2), ...
                data_struct(i).img_1_radii, data_struct(i).pixel_spacing);
            result_struct.(strcat('phantom_diam_5_',short_seq_name)) = ACR_geo_acc_ax( data_struct(i).img_5_filtered, params_struct, ...
                data_struct(i).img_5_centercoords(1), data_struct(i).img_5_centercoords(2), ...
                data_struct(i).img_5_radii, data_struct(i).pixel_spacing);
           
            %% High contrast spatial resolution
            result_struct.(strcat('res_',short_seq_name)) = ACR_high_cont_spat_res( data_struct(i).img_1, params_struct, ...
                data_struct(i).img_1_centercoords(1), data_struct(i).img_1_centercoords(2), ...
                data_struct(i).pixel_spacing, params_struct.interpolation_factor);
           
            %% Slice Thickness
            result_struct.(strcat('slicethickness_',short_seq_name)) = ACR_slice_thick(data_struct(i).img_1_rot, params_struct, ...
                data_struct(i).img_1_centercoords(1), data_struct(i).img_1_centercoords(2), data_struct(i).pixel_spacing);
           
            %% Percent Intensity Uniformity
            result_struct.(strcat('piu_',short_seq_name)) = ACR_bf_estimation(data_struct(i).img_7_filtered, params_struct, ...
                data_struct(i).img_7_centercoords(1), data_struct(i).img_7_centercoords(2), data_struct(i).pixel_spacing);
            
            %% Percent Signal Ghosting
            result_struct.(strcat('psg_',short_seq_name)) = ACR_ghosting(data_struct(i).img_7, params_struct, ...
               data_struct(i).img_7_centercoords(1), data_struct(i).img_7_centercoords(2), ...
               data_struct(i).img_7_radii, data_struct(i).pixel_spacing);
           
            %% Signal to Noise Ratio
            result_struct.(strcat('snr_',short_seq_name)) = ACR_snr_within_phantom(data_struct(i).img_7, params_struct, ...
               data_struct(i).img_7_centercoords(1), data_struct(i).img_7_centercoords(2), ...
               data_struct(i).img_7_radii, data_struct(i).pixel_spacing);
        end
    end
end

function result = GoesInACRSeq(files_struct, info, seq_name)
    result = false;
    if(~isempty(fieldnames(files_struct)))
        cpt_seq = find(arrayfun(@(n) strcmp(files_struct(n).seq_name, seq_name), 1:numel(files_struct)),1);
        if(isempty(cpt_seq) || ...
           (~isempty(find(cpt_seq,1)) && ...
            strcmp(files_struct(find(arrayfun(@(n) strcmp(files_struct(n).seq_name, seq_name), 1:numel(files_struct)))).seq_id, info.SeriesInstanceUID)))
            result = true;
        end
    end
    
end

function files_struct = AddFileToStruct(full_file_name, info, files_struct, sequence_name)
    cpt_seq = [];
    
    if(isfield(files_struct, 'seq_id'))
        cpt_seq = find(arrayfun(@(n) strcmp(files_struct(n).seq_id, info.SeriesInstanceUID), 1:numel(files_struct)));
    end
    
    % Create structure for serie if does not exist yet
    if (isempty(cpt_seq) || isempty(files_struct))
        cpt_seq = size(files_struct,2);
        if(isfield(files_struct, 'seq_id'))
            cpt_seq = cpt_seq + 1;
        end
        
        files_struct(cpt_seq).seq_id = info.SeriesInstanceUID;
        if(~isempty(sequence_name))
            files_struct(cpt_seq).seq_name = sequence_name;
        else
            files_struct(cpt_seq).seq_name = info.SeriesDescription;
        end
        files_struct(cpt_seq).patient_id = info.PatientID;
        files_struct(cpt_seq).mri_id = info.StationName;
    end
    
    % Fill structure
    switch info.InstanceNumber
        case 1
            files_struct(cpt_seq).full_file_name_1 = full_file_name;
        case 5
            files_struct(cpt_seq).full_file_name_5 = full_file_name;
        case 7
            files_struct(cpt_seq).full_file_name_7 = full_file_name;
        case 11
            files_struct(cpt_seq).full_file_name_11 = full_file_name;
        case 12
            files_struct(cpt_seq).full_file_name_1 = full_file_name;
        case 16
            files_struct(cpt_seq).full_file_name_5 = full_file_name;
        case 18
            files_struct(cpt_seq).full_file_name_7 = full_file_name;
        case 22
            files_struct(cpt_seq).full_file_name_11 = full_file_name;
    end
    
    
end

function [has_acr_loc, has_acr_t1, has_acr_t2] = HasACRFiles(files_struct)
    has_acr_loc = 0;
    has_acr_t1 = 0;
    has_acr_t2 = 0;
    
    if(isempty(fieldnames(files_struct)))
        return;
    end
    
    cpt_seq = find(arrayfun(@(n) strcmp(files_struct(n).seq_name, 'ACR_Axial_T1'), 1:numel(files_struct)));
    if(~isempty(cpt_seq) && exist(files_struct(cpt_seq).full_file_name_1) == 2 && ...
            exist(files_struct(cpt_seq).full_file_name_5) == 2 && ...
            exist(files_struct(cpt_seq).full_file_name_7) == 2 && ...
            exist(files_struct(cpt_seq).full_file_name_11) == 2)
        has_acr_t1 = 1;
    end

    cpt_seq = find(arrayfun(@(n) strcmp(files_struct(n).seq_name, 'ACR_Axial_T2'), 1:numel(files_struct)));
    if(~isempty(cpt_seq) && exist(files_struct(cpt_seq).full_file_name_1) == 2 && ...
            exist(files_struct(cpt_seq).full_file_name_7) == 2 && ...
            exist(files_struct(cpt_seq).full_file_name_11) == 2)
        has_acr_t2 = 1;
    end

    cpt_seq = find(arrayfun(@(n) strcmp(files_struct(n).seq_name, 'ACR_Sagittal_Loc'), 1:numel(files_struct)));
    if(~isempty(cpt_seq) && exist(files_struct(cpt_seq).full_file_name_1) == 2)
        has_acr_loc = 1;
    end
    
end

function [we_can_liftoff] = ValidateData(files_struct, params_struct)
    % Validate data for all series
    for i=1:length(files_struct)    
              
        img_1 = dicomread(files_struct(i).full_file_name_1);
        info_1 = dicominfo(files_struct(i).full_file_name_1);
        ps = info_1.PixelSpacing;
        
        %% Pixel isotropy
        if(ps(1) ~= ps(2))
            we_can_liftoff = 0;
            error(strcat('Pixels are not isotrope for sequence', files_struct(i).seq_name));
            return;
        end
        
        %% SNR  
        snr = ACR_snr_from_bg(img_1, info_1);
        if(snr <  params_struct.min_snr)
            we_can_liftoff = 0;
            error(strcat('The background SNR is below the limit for sequence ', files_struct(i).seq_name));
            return;
        end

        %% Phantom center vs FOV
        % Sagittal
        if(strcmp(files_struct(i).seq_name, 'ACR_Sagittal_Loc'))        
            img_sag = dicomread(files_struct(i).full_file_name_1);
            img_sag_bw = zeros(size(img_sag));
            img_sag_bw(img_sag(:)>max(img_sag(:)/3)) = 1;
            vct_sag_bw_x = sum(img_sag_bw,1);
            vct_sag_bw_y = sum(img_sag_bw,2);
            if(vct_sag_bw_x(1) > 0 || vct_sag_bw_x(end) > 0 || vct_sag_bw_y(1) > 0 || vct_sag_bw_y(end) > 0)
                we_can_liftoff = 0;
                error(strcat('Part of the phantom is outside the FoV for sequence', files_struct(i).seq_name));
                return;
            end
        % Axial
        else
            img_11 = dicomread(files_struct(i).full_file_name_11);
            
            img_1_bw = zeros(size(img_1));
            img_11_bw = zeros(size(img_11));
            img_1_bw(img_1(:)>max(img_1(:)/3)) = 1;
            img_11_bw(img_11(:)>max(img_11(:)/3)) = 1;

            vct_1_bw_x = sum(img_1_bw,1);
            vct_11_bw_x = sum(img_11_bw,1);
            vct_1_bw_y = sum(img_1_bw,2);
            vct_11_bw_y = sum(img_11_bw,2);

            if((vct_1_bw_x(1) > 0 || vct_1_bw_x(end) > 0 || vct_1_bw_y(1) > 0 || vct_1_bw_y(end) > 0) || ...
                    (vct_11_bw_x(1) > 0 || vct_11_bw_x(end) > 0 || vct_11_bw_y(1) > 0 || vct_11_bw_y(end) > 0))
                we_can_liftoff = 0;
                error(strcat('Part of the phantom is outside the FoV for sequence', files_struct(i).seq_name));
                return;
            end
            
            %% Rotation angle
            % Find phantom circle on slice 1 and 11
            h = fspecial('gaussian', params_struct.noise_filter_kernel_size_px, params_struct.noise_filter_kernel_std_px); 

            img_1_filtered = imfilter(img_1,h);
            [accum_gros, circen_gros, cirrad_gros] = ...
            CircularHough_Grd(img_1_filtered, [round(params_struct.phantom_diam_mm/2/ps(1)*0.98) round(params_struct.phantom_diam_mm/2/ps(1)*1.02)]);
            phantom_center_coords_1_px = circen_gros;

            img_11_filtered = imfilter(img_11,h);
            [accum_gros, circen_gros, cirrad_gros] = ...
            CircularHough_Grd(img_11_filtered, [round(params_struct.phantom_diam_mm/2/ps(1)*0.98) round(params_struct.phantom_diam_mm/2/ps(1)*1.02)]);
            phantom_center_coords_11_px = circen_gros;

            % Axis rotation angle
            [img_rot, phantom_axis_rot_angle_deg] = ACR_rot_angle(img_1_filtered, ps(1), ...
                phantom_center_coords_1_px(1), phantom_center_coords_1_px(2), params_struct.interpolation_factor);

            if(abs(phantom_axis_rot_angle_deg) > params_struct.max_phantom_axis_rotation_angle)
                we_can_liftoff = 0;
                error(strcat('The phantom axis rotation angle is above the limit for sequence', files_struct(i).seq_name));
                return;
            end

            % Off-Axis rotation angle (approximate measurement as it does not
            % account for image distorsion
            dist_z_mm = 100;
            dist_x_px = abs(phantom_center_coords_11_px(1) - phantom_center_coords_1_px(1));
            dist_x_mm = dist_x_px*ps(1);
            dist_y_px = abs(phantom_center_coords_11_px(2) - phantom_center_coords_1_px(2));
            dist_y_mm = dist_y_px*ps(1);
            dist_axis_mm = sqrt(dist_x_mm^2+dist_y_mm^2);
            phantom_offaxis_rot_angle_deg = atan(dist_axis_mm/dist_z_mm);

            if(abs(phantom_offaxis_rot_angle_deg) > params_struct.max_phantom_offaxis_rotation_angle)
                we_can_liftoff = 0;
                error(strcat('The phantom offaxis rotation angle is above the limit for sequence', files_struct(i).seq_name));
                return;
            end
            
            %% Grid positionning - Longitudinal
            grid_longi_pos_mm = ACR_slice_pos(img_rot, params_struct, phantom_center_coords_1_px(1),...
                phantom_center_coords_1_px(2), ps(1));

            if(abs(grid_longi_pos_mm) > params_struct.max_longi_grid_pos_offset_mm)
                we_can_liftoff = 0;
                error(strcat('The acquisition grid has a longitudinal offset above the limit for sequence', files_struct(i).seq_name));
                return;
            end
            
        end
    end
    
    we_can_liftoff = 1;
end

function [data] = DataPrep(files_struct, params_struct)
    data = struct;
    
    h = fspecial('gaussian', params_struct.noise_filter_kernel_size_px, params_struct.noise_filter_kernel_std_px); 
    
    % boucler sur les s�ries
    for i = 1:length(files_struct)
        dinfo = dicominfo(files_struct(i).full_file_name_1);
        data(i).pixel_spacing = dinfo.PixelSpacing;
        data(i).dttm = [dinfo.SeriesDate(7:8), '-', dinfo.SeriesDate(5:6), ...
            '-', dinfo.SeriesDate(1:4), ' ', dinfo.SeriesTime(1:2), ':', dinfo.SeriesTime(3:4)];
        data(i).patient_id = files_struct(i).patient_id;
        data(i).mri_id = files_struct(i).mri_id;
        data(i).imaging_freq = dinfo.ImagingFrequency;
        data(i).seq_name = files_struct(i).seq_name;
        
        % Si ACR Sagittal
        if(strcmp(files_struct(i).seq_name, 'ACR_Sagittal_Loc'))
            % Extraire l'image, son dcminfo et la filtrer
            data(i).img_1 = dicomread(files_struct(i).full_file_name_1);
            data(i).img_1_filtered = imfilter(data(i).img_1, h);
        % Si ACR Axial
        else
            data = ExtractImgData(files_struct(i),data, i, 1, params_struct, dinfo, h);
            data = ExtractImgData(files_struct(i),data, i, 5, params_struct, dinfo, h);
            data = ExtractImgData(files_struct(i),data, i, 7, params_struct, dinfo, h);
            data = ExtractImgData(files_struct(i),data, i, 11, params_struct, dinfo, h);        
        end
    end  
end

function [data] = ExtractImgData(file_struct, data, indice, img_num, params_struct, dinfo, h)
    data(indice).(strcat('img_', num2str(img_num))) = dicomread(file_struct.(strcat('full_file_name_', num2str(img_num))));
    data(indice).(strcat('img_', num2str(img_num), '_filtered')) = imfilter(data(indice).(strcat('img_', num2str(img_num))), h);
    [accum_gros, circen_gros, cirrad_gros] = ...
    CircularHough_Grd(data(indice).(strcat('img_', num2str(img_num), '_filtered')), [round(params_struct.phantom_diam_mm/2/dinfo.PixelSpacing(2)*0.90) round(params_struct.phantom_diam_mm/2/dinfo.PixelSpacing(2)*1.2)]);
    data(indice).(strcat('img_', num2str(img_num), '_centercoords')) = circen_gros;
    data(indice).(strcat('img_', num2str(img_num), '_radii')) = cirrad_gros;
    
    % If we have 1st slice, we calculate the rot angle and rotate the
    % image. Otherwise, we take slice 1 rotation angle and rotate the image
    % accordingly.
    if(img_num == 1)
        [data(indice).(strcat('img_', num2str(img_num), '_rot')), data(indice).angle_rot] = ACR_rot_angle(data(indice).(strcat('img_', num2str(img_num), '_filtered')), dinfo.PixelSpacing(2), circen_gros(1), circen_gros(2), params_struct.interpolation_factor);
    else
        data(indice).(strcat('img_', num2str(img_num), '_rot')) = imrotate(data(indice).(strcat('img_', num2str(img_num), '_filtered')),data(indice).angle_rot,'bilinear','crop');
    end
    
end



