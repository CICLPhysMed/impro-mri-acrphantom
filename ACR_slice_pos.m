%% Par: Bruno Carozza
%% Cr�� le: 26 mars 2015
%% Description: D�termine la position d'une tranche pour s'assurer que le
%%              fant�me est bien plac� par rapport � la grille d'acquisition
%% R�sultats: diff�rence de longueure entre les barres
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function slice_pos = ACR_slice_pos(img_rot, params_struct, pos_cercle_x, pos_cercle_y, ps)
    
    %%Examiner, � partir du centre (un peu plus haut) le signal d'intensit�
    %%du c�t� gauche et droit de l'axe centrale et d�terminer l'endroit o�
    %%il y a bordure entre le liquide dans le fant�me et les deux objets angulaires (45 degrees wedges) 
    
    min_x = round(pos_cercle_x + params_struct.element_slicepos_boxminx_mm/ps);
    max_x = round(pos_cercle_x + params_struct.element_slicepos_boxmaxx_mm/ps);
    min_y = round(pos_cercle_y + params_struct.element_slicepos_boxminy_mm/ps);
    max_y = round(pos_cercle_y + params_struct.element_slicepos_boxmaxy_mm/ps);
    
    %Vectoriser l'information utile
    data = img_rot(min_y:max_y,min_x:max_x);
    vct1   = sum(data(:,1:round(size(data,2)/2)-1),2);  
    vct2   = sum(data(:,round(size(data,2)/2)+1:end),2);  
    
    % Ramener � 0 la moiti� de la hauteure de la courbe
    vct1 = vct1 - (max(vct1)+min(vct1))/2;
    vct2 = vct2 - (max(vct2)+min(vct2))/2;
    
    % Faire un fit de smoothing
    f1 = fit((1:length(vct1))', vct1,  'smoothingspline', 'SmoothingParam', 0.004);
    f2 = fit((1:length(vct2))', vct2,  'smoothingspline', 'SmoothingParam', 0.004);
 
    % Obtenir les positions de la fonction qui croisent la moiti�
    % d'intensit� du signal max-min.
    poss = newtzero(f1, [], 20, 0.0001);
    poss = poss(poss > 0 & poss < length(vct1));
    pos1 = poss(1);
    poss = newtzero(f2, [], 20, 0.0001);
    poss = poss(poss > 0 & poss < length(vct2));
    pos2 = poss(1);
    
  
    
    slice_pos = (pos2 - pos1)*ps;
    
%     results.result = abs((pos1 - pos2)*ps); % 
%     plotdata = zeros([2 2]);
%     plotdata(1,1) = (min_x + (round(size(data,2)/2)-1)/2);
%     plotdata(2,1) = (min_x + 3/4*(max_x-min_x));
%     plotdata(1,2) = (min_y + pos1);
%     plotdata(2,2) = (min_y + pos2);
    
%     f = figure;
%     imagesc(img_rot)
%     hold on
%     %imagesc(flipud(ImageData.Img),'Border','tight');
%     colormap gray;
%     plot(plotdata(:,1), plotdata(:,2), 'o', 'color', [1 1 0], 'linewidth',1);
%     axis off
%     axis equal
%     hold off
    
    
end