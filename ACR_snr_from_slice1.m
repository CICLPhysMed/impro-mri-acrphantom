function snr = ACR_snr_from_bg(img, info)

    %filepath = find_slice_num('C:\Users\carbru\Google Drive\Dev\ACR_MRI_PhantomAnalysis\data\Acceptation\slice_thickness', 5, '3.0 mm Axial T2');
    
    info = dicominfo(filepath);
    img = dicomread(filepath);
    
    % Extraire les caract�ristiques de l'image    
    img_ps = info.PixelSpacing;
    img_size = size(img);    
    
    ftm_diametre_mm = 190;
    ftm_diametre_px = ftm_diametre_mm/img_ps(2);
    
    % Filtrer le bruit dans l'image
    h = fspecial('gaussian', [7 7], 2); 
    img_filtered = imfilter(img,h); 
    
    %% Recherche du cercle id�al
    [accum_gros, circen_gros, cirrad_gros] = ...
    CircularHough_Grd(img_filtered, [round(ftm_diametre_mm/2/img_ps(2)*0.95) round(ftm_diametre_mm/2/img_ps(2)*1.1)]);

    pos_cercle_x = circen_gros(1);
    pos_cercle_y = circen_gros(2);
    ray_cercle   = cirrad_gros;

    % Define signal box
    min_x = round(pos_cercle_x - 15);
    max_x = round(pos_cercle_x + 15);
    min_y = round(pos_cercle_y - 50);
    max_y = round(pos_cercle_y - 20);
    
    data = img(min_y:max_y, min_x:max_x);
    
    % Find the average signal 
    moy_signal = median(data(data(:) > max(data(:))*2/3));

   
    
    img_noise_SD            = std(double(data(:)));
    img_noise               = img_noise_SD;%/0.66; > seulement lorsqu'on prend le bruit dans � un endroit sans signal > dist ricienne
    
    snr                     = double(moy_signal)/img_noise;
    
end