%% Par: Bruno Carozza
%% Cr�� le: 2 avril 2015
%% Description: D�termine le Percent Uniformity Intensity dans le fant�me
%% R�sultats: PUI
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function percent_integral_uniformity = ACR_bf_estimation(img_7_filtered, params_struct, pos_cercle_x, pos_cercle_y, ps)

    img_size = size(img_7_filtered);
    
    %%Trouver un rayon en pixels pour lequel la surface du cercle de m�me
    %%centre sera d'environ 200 cm^2 ou 20 000 mm^2. 
    pi = 3.141592;
    r = sqrt(20000/ps/pi)*ps;
    
    %%D�terminer des points d�finissants le contour de ce cercle pour cr�er
    %%un masque
    [H,X,Y] = circle([pos_cercle_x pos_cercle_y],r,1000,'-w',0);
    
    %%Cr�er un masque � partir du centre du fant�me et du nouveau rayon
    %%ayant la forme circulaire. Les voxels appartenants � ce masque seront
    %%ceux utilis�s pour d�terminer l'uniformit� de l'intensit�... ou
    %%plut�t le bias field (BF)
    ROI = roipoly(1:img_size(1), 1:img_size(2), img_7_filtered, X, Y);
    
    %Prendre le log des data
    ROI_data = (img_7_filtered(ROI));
    
    
    %%fitter l'inhomog�n�it� du signal par un polynome 2D de degr� 3 ou 4
    %%(Van Leemput) afin d'en tirer la valeure maximal et minimale du
    %%signal sans tenir compte du bruit.
    pos = [];
    [pos(:,1),pos(:,2)] = find(ROI);    
    
    
    % Trouver le poids d'appartenance de chaque pixel au ROI
    param = fitdist(double(ROI_data), 'rician');
    pd = makedist('Rician', 's', param.s, 'sigma', param.sigma);
    temp = pdf(pd, double(ROI_data));
    w = temp./max(temp);
    
    %w  = ones([sum(ROI(:)>0) 1]);      
    v  = var(single(ROI_data));
    
    
    %%Calcul du mean signal intensity pour la d�termination du
    %%"percent signal ghosting"
    ROI_mean_signal = sum(double(ROI_data(:,1)).*w(:,1))/sum(w);
    
    mu = ROI_mean_signal;
    
    % pour la normalisation du champ biais�
    ytnum = single(ROI_data)/v*mu; 
    ytdeno = single(ROI_data)/v;

    yt = sum(ytnum,2)./(sum(ytdeno,2)+eps);
    
    
    p = polyfitweighted2(single(pos(:,1)),single(pos(:,2)),single(ROI_data),single(yt),6,single(w));
    if sum(isnan(p))>0
        p = zeros([10 1]);
    end
    bf = polyval2(p,single(pos(:,1)),single(pos(:,2)));
    
    min_bf = min(bf);
    max_bf = max(bf);
    
    percent_integral_uniformity = 100 * (1 - ((max_bf-min_bf)/(max_bf+min_bf+2*ROI_mean_signal)));
        
end