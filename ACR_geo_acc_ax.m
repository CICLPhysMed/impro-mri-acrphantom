%% Par: Bruno Carozza
%% Cr�� le: 24 mars 2015
%% Modifi� le: 13 mai 2015
%% Description: cette fonction prend en entr�e un fichier DICOM IRM ACR T1
%%              en axial et d�termine son diam�tre (le plus diff�rent de 
%%              celui attendu.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function diam_pire_mm = ACR_geo_acc_ax(img, params_struct, pos_cercle_x, pos_cercle_y, radii, ps )
    
    img_size = size(img);    

    ftm_diametre_px = params_struct.phantom_diam_mm/ps(2);
    
    % Remplir le fant�me pour faciliter l'identification de la bordure    
    img_bw = img;
    img_bw(img(:) < mean(img(:))/1.5) = 0;
    img_bw(img(:) > mean(img(:))/1.5) = 1;
    se = strel('disk',round(7/ps(1)));
    img_bw = imclose(img_bw,se);
    img_bw = imopen(img_bw,se);
    se = strel('disk',round(4/ps(1)));
    img_bw = imerode(img_bw,se);
    %img_filled = img_filtered;
    img_filled = MidpointCircle(img, radii*0.98, pos_cercle_y, pos_cercle_x, mean(img(img_bw(:)==1)));
    img_filled(img_bw(:)==1) = mean(img(img_bw(:)==1))*0.7;
    
    %% Regarder la bordure du cercle dans la direction radiale pour
    %% d�terminer l'endroit exact de la bordure - r�ponse subvoxelique.
    %nbPoints = 360;
    nbPoints = 180*3;
    loadlibrary('ImageProcessing.dll', @ImageProcessing);
    for i=1:nbPoints        
        %Angle = -pi/3 + i*2*pi/3/nbPoints;
        Angle = i*pi/nbPoints;
        edge_coords_PTR = calllib('ImageProcessing','RadialEdgeDetection',img_filled ,img_size,pos_cercle_x,pos_cercle_y,radii, Angle);
        setdatatype(edge_coords_PTR,'doublePtr',3,1);
        edge_coords = get(edge_coords_PTR);
        edge_coords_x(i) = edge_coords.Value(1);
        edge_coords_y(i) = edge_coords.Value(2);
        edge_coords_rad(i) = edge_coords.Value(3);
        
        % Angle oppos�
        Angle = Angle + pi;
        edge_coords_PTR = calllib('ImageProcessing','RadialEdgeDetection',img_filled ,img_size,pos_cercle_x,pos_cercle_y,radii, Angle);
        setdatatype(edge_coords_PTR,'doublePtr',3,1);
        edge_coords = get(edge_coords_PTR);
        edge_coords_x(i + nbPoints) = edge_coords.Value(1);
        edge_coords_y(i + nbPoints) = edge_coords.Value(2);
        edge_coords_rad(i + nbPoints) = edge_coords.Value(3);
    end
    unloadlibrary 'ImageProcessing';
       
    
    %%Comparer le diam�tre observ� avec le diam�tre r�el du fant�me
    diam_pire_px = ftm_diametre_px;
        
    diam = edge_coords_rad(1:end/2) + edge_coords_rad(size(edge_coords_rad,2)/2+1:end);
    f = fit((1:nbPoints)', diam',  'smoothingspline', 'SmoothingParam', 0.0004);
    
    for i=nbPoints/3:nbPoints/3*2
        if(abs(diam_pire_px - ftm_diametre_px) < abs(f(i) - ftm_diametre_px))
            diam_pire_px = f(i);
        end
    end
    
    diam_pire_mm = diam_pire_px.*ps(1);  
    
%     subplot(1,2,1);
%     hold on
%     imagesc(img_filled)
%     plot(edge_coords_x,edge_coords_y,'g', 'linewidth', 2)
%     hold off
%     subplot(1,2,2);
%     hold on
%     plot(diam(nbPoints/3+1:nbPoints/3*2));
%     plot(f(nbPoints/3+1:nbPoints/3*2), '-r');
%     hold off
%     
%     diam = f(nbPoints/3+1:nbPoints/3*2);
    
     
    
%     %sag et coronal
%     diam(90)*img_ps(1)
%     diam(45)*img_ps(1)
%     diam(1)*img_ps(1)
%     diam(135)*img_ps(1)
%     %axial    
%     diam(90)*img_ps(1)
%     diam(135)*img_ps(1)
%     diam(1)*img_ps(1)
%     diam(45)*img_ps(1)
    
    
end