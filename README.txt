﻿CONTENU DE CE FICHIER
---------------------
   
 * Introduction
 * Prérequis
 * Utilisation
 * Auteur / contact
 
 
INTRODUCTION
------------

**imPro MRI ACRPhantom (Open source - License MIT)**

Copyright 2015 Centre Intégré de santé et de services sociaux de Laval.

imPro MRI ACRPhantom est un outil développé sur Matlab et C++ qui effectue automatiquement l'analyse 
du fantôme IRM d'accréditation de l'American College of Radiology. L'objectif de l'outil est 
d'automatiser les tests décrits dans le documents de l'ACR: 

http://www.acr.org/~/media/ACR/Documents/Accreditation/MRI/LargePhantomGuidance.pdf

Vous êtes invités à contribuer au projet sur https://bitbucket.org/CICLPhysMed/impro-mri-acrphantom

Plus d'informations dans le fichiers DETAILS.TXT.


PRÉREQUIS
------------
 * Cet outil a été testé sur les versions r2013b, r2014a, r2014b et r2015a de Matlab. 
 * Visual C++ redistributable packages for Visual Studio 2013 doit être installé sur le poste pour
	le fonctionnement de la librairie ImageProcessing.dll.

	
UTILISATION
------------
 1. Déposer au moins une série d'images dans un dossier.
 2. Ajouter le dossier (et sous dossiers) contenant le code au path Matlab.
 3. Lancer la fonction iPro_mri_acrphantom('C:\dosier') en spécifiant le chemin du dossier.
 4. L'exécution peut prendre jusqu'à 30 secondes dépendant des performances de l'ordinateur.
 5. Les résultats sont affichés dans le fil de Matlab à la fin.
 
AUTEUR / CONTACT
----------------
Bruno Carozza M.Sc. 
Physicien-informaticien
Département de radio-oncologie
Centre intégré de cancérologie de Laval
1755 Boul. René-Laennec
Laval (Québec) H7M 3L9

Téléphone: 450 668-1010 x24653
Courriel: bcarozza.csssl@ssss.gouv.qc.ca