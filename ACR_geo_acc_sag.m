%% Par: Bruno Carozza
%% Cr�� le: 23 mars 2015
%% Modifi� le: 13 mais 2015
%% Description: cette fonction prend en entr�e un fichier DICOM IRM ACR T1
%%              en sagital (localizer) et d�termine sa hauteur tout juste �
%%              gauche du milieu
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ phantom_height ] = ACR_geo_acc_sag( img, img_ps)
    
    img_size = size(img);    
    
    % Caract�riser l'intensit� du fond
    img_noise_roitl = img(2:round(25/img_ps(1)),2:round(25/img_ps(2)));
    img_noise_roibl = img(img_size(1)-round(25/img_ps(1)):img_size-1,2:round(25/img_ps(2)));
    img_noise_roitr = img(2:round(25/img_ps(1)),img_size(2)-round(25/img_ps(2)):img_size(2)-1);
    img_noise_roibr = img(img_size(1)-round(25/img_ps(1)):img_size-1,img_size(2)-round(25/img_ps(2)):img_size(2)-1);
    img_noise_roi = single([img_noise_roitl(:)' img_noise_roibl(:)' img_noise_roitr(:)' img_noise_roibr(:)']');
        
    % Nettoyer l'image
    img_bw = img;
    img_bw(:) = img(:) > max(img_noise_roi(:))*3;
    
    % Trouver le centre du fant�me
    CC = bwconncomp(img_bw);
    S = regionprops(CC, 'Area');
    L = labelmatrix(CC);
    P = round(img_size(1) * img_size(2) / 8);
    img_bw2 = ismember(L, find([S.Area] >= P));
    props = regionprops( double( img_bw2 ), 'Centroid' ); 
    
    % Se tasser de 23mm vers la gauche et prendre le vecteur d'intensit� en
    % Y
    vct = img(:, round(props.Centroid(1)-23/img_ps(2)));
        
    pos1 = EdgeDetect1D(vct,1);
    pos2 = EdgeDetect1D(vct,0);
    
    height_vox = pos2 - pos1; 
    height_mm = height_vox*img_ps(1);
    
    phantom_height = height_mm;
    
    
end

