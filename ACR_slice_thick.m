%% Par: Bruno Carozza
%% Cr�� le: 25 mars 2015
%% Modifi� le 14 mai 2015
%% Description: cette fonction mesure la longueure des rampes permettant de
%%              mesurer l'�paiseur de la tranche
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Slice_Thickness] = ACR_slice_thick(img_rot, params_struct, pos_cercle_x, pos_cercle_y, ps)
     
    % Cr�er un masque de la r�gion rectangulaire centrale
    mask_top = round(pos_cercle_y - 13/ps(1));
    mask_btm = round(pos_cercle_y + 13/ps(1));
    mask_left = round(pos_cercle_x - 87/ps(2));
    mask_right = round(pos_cercle_x + 87/ps(2));
    img_mask = img_rot(mask_top:mask_btm, mask_left:mask_right);
    
    % Isoler les bandes rectangulaires dans lesquels les wedges 
    % de slice thickness sont pr�sents
    object_lines = edge(int16(img_mask),'canny'); % extract edges
    rect_limits = sum(object_lines,2);
    pos_temp = find(rect_limits(1:round(size(rect_limits,1)/2)) == max(rect_limits(1:round(size(rect_limits,1)/2))));
    PosMinY = mask_top + pos_temp+2;
    pos_temp = find(rect_limits(round(size(rect_limits,1)/2)+1:end) == max(rect_limits(round(size(rect_limits,1)/2)+1:end)));
    PosMaxY = mask_top + round(13/ps(1)) + pos_temp-2;
        
    demie_largeure_rect = 83*0.97/ps(1);
    PosMinX = round(pos_cercle_x - demie_largeure_rect);
    PosMaxX = round(pos_cercle_x + demie_largeure_rect);

    PosMinX =  PosMinX+1;
    PosMaxX = PosMaxX-1 ;  
    img_rect = img_rot(PosMinY:PosMaxY,PosMinX:PosMaxX);       
    sum_col = sum(img_rect,2);    
    % S'assurer qu'il s'agit bel et bien d'une rang�e contenant le
    % contraste pour d�terminer l'�paisseur de la tranche.
    nb_row = size(sum_col,1);
    moy_row = sum(sum_col)/nb_row;
    seuil = 0.75*moy_row;
    valid_rows   = sum_col>seuil;
    valid_rows = repmat(valid_rows,1,size(img_rect,2));
    img_rect = double(valid_rows).*double(img_rect);


    marge = round(1/ps(1));
    data1 = sum(img_rect(1:round(nb_row/2-1),marge:size(img_rect,2)-marge));
    data2 = sum(img_rect(round(nb_row/2+1):end,marge:size(img_rect,2)-marge));

    % Ramener � 0 la moiti� de la hauteure de la courbe
    data1 = data1 - (max(data1)+min(data1))/2;
    data2 = data2 - (max(data2)+min(data2))/2;
    
    % Faire un fit de smoothing
    f1 = fit((1:length(data1))', data1',  'smoothingspline', 'SmoothingParam', 0.004);
    f2 = fit((1:length(data2))', data2',  'smoothingspline', 'SmoothingParam', 0.004);
 
    % Obtenir les positions de la fonction qui croisent la moiti�
    % d'intensit� du signal max-min.
    poss = newtzero(f1, [], 20, 0.0001);
    poss = poss(poss > 0 & poss < length(data1));
    pos1_1 = poss(1);
    pos1_2 = poss(2);
    ramp1_larg = (pos1_2 - pos1_1)*ps(1);
    
    poss = newtzero(f2, [], 20, 0.0001);
    poss = poss(poss > 0 & poss < length(data2));
    pos2_1 = poss(1);
    pos2_2 = poss(2);
    ramp2_larg = (pos2_2 - pos2_1)*ps(1);
    
    Slice_Thickness = 0.2*(ramp1_larg*ramp2_larg)/(ramp1_larg+ramp2_larg);
end